const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/



const data = Object.entries(favouritesMovies);


// Q1. Find all the movies with total earnings more than $500M.


const answer1 = data.filter((entries) => {

    let tempstr = entries[1].totalEarnings;
    tempstr = (tempstr.slice(1, 4));
    return tempstr > 500;
});

console.log(answer1);





// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.


const answer2 = answer1.filter((entries) => {

    return entries[1].oscarNominations > 3;
});


console.log(answer2);






// Q.3 Find all movies of the actor "Leonardo Dicaprio".

const answer3 = data.filter((entries) => {

    let actors_Arr = entries[1].actors;
    if (actors_Arr.includes('Leonardo Dicaprio')) {
        return true;
    }
    else {
        return false;
    }
});

console.log(answer3);





// Q:4 Sort movies (based on IMDB rating) if IMDB ratings are same, compare totalEarning as the secondary metric.

const answer4 = data.sort((prev, cur) => {


    if (prev[1].imdbRating > cur[1].imdbRating) {
        return -1;
    }
    else if (prev[1].imdbRating < cur[1].imdbRating) {
        return 1;
    }
    else {
        let prev_Earning = prev[1].totalEarnings;
        prev_Earning = Number(prev_Earning.slice(1, 4));

        let cur_Earning = cur[1].totalEarnings;
        cur_Earning = Number(cur_Earning.slice(1, 4));
        if (prev_Earning > cur_Earning) {
            return -1;
        }
        else if (prev_Earning < cur_Earning) {
            return 1;
        }
        else {
            return 0;
        }
    }
});

console.log(answer4);






// Question 5 : Group movies based on genre. Priority of genres in case of multiple genres present are : drama > sci-fi > adventure > thriller > crime


const genres = ["drama", "sci-fi", "adventure", "thriller", "crime"];


const answer5 = Object.fromEntries(genres.map((genre) => {
    return [genre, []];
}));

const demo = data.map(([movie, details]) => {
    const genres1 = details.genre;
    const found = genres.find((genre) => {
        return genres1.includes(genre);
    });

    if (found) {
        answer5[found].push(movie);
    }
});

console.log(answer5);



